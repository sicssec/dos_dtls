/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Stefan Jucker - DTLS implementation
 *    Kai Hudalla (Bosch Software Innovations GmbH) - small improvements
 ******************************************************************************/
package org.eclipse.californium.scandium.dtls;

import org.eclipse.californium.scandium.util.DatagramReader;
import org.eclipse.californium.scandium.util.DatagramWriter;
import sun.security.krb5.internal.crypto.Nonce;


public final class HandshakeTokenExtension extends HelloExtension {

	// DTLS-specific constants ////////////////////////////////////////

	//private static final int NONCE_BITS = 32;

	private static final int TOKEN_HANDSHAKE_BITS = 320;
	/** The handshake token */
	private byte[] handshakeToken;
	//private byte[] Nonce;

	// Constructor ////////////////////////////////////////////////////

	public HandshakeTokenExtension() {
		super(ExtensionType.HANDSHAKE_TOKEN);
		this.handshakeToken = null;
	//	this.Nonce = Nonce;
	}

	public HandshakeTokenExtension(byte[] token) {
		super(ExtensionType.HANDSHAKE_TOKEN);
		this.handshakeToken = new byte[TOKEN_HANDSHAKE_BITS / 8];
		//this.Nonce = new byte[4];
		System.arraycopy(token, 0, this.handshakeToken, 0, TOKEN_HANDSHAKE_BITS / 8);
		//System.arraycopy(Nonce, 0, this.Nonce, 0, 4);
		//this.Nonce = Nonce;
	}
	
	// Serialization //////////////////////////////////////////////////

	@Override
	protected void addExtensionData(DatagramWriter writer) {
		int extLength = (TOKEN_HANDSHAKE_BITS) / 8;
		writer.write(extLength, LENGTH_BITS);
		if (handshakeToken != null)
		    writer.writeBytes(handshakeToken);
	}

	public static HelloExtension fromExtensionData(byte[] extensionData) {
		DatagramReader reader = new DatagramReader(extensionData);

		byte[] token = reader.readBytes(TOKEN_HANDSHAKE_BITS / 8);

		return new HandshakeTokenExtension(token);
	}
	
	// Methods ////////////////////////////////////////////////////////
	
	@Override
	public int getLength() {
		// fixed: type (2 bytes), length (2 bytes), token (0/32 bytes)
		int tokenSize = handshakeToken.length;
		return 4 + tokenSize;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append("\t\t\t\tLength: " + (getLength() - 4) + "\n");
		
		sb.append("\t\t\t\tHandshake Token : ");
		for (byte bi : handshakeToken)
		    sb.append(String.format("%02x", bi&0xff));
		sb.append("\n");

		return sb.toString();
	}

	public byte[] getToken() {
		return this.handshakeToken;
	}
	
	public void setToken(byte[] token) {
		if (token == null)
		      this.handshakeToken = null;
		else {
			this.handshakeToken = new byte[TOKEN_HANDSHAKE_BITS / 8];
			System.arraycopy(token, 0, this.handshakeToken, 0, TOKEN_HANDSHAKE_BITS / 8);
		}
	}

}
