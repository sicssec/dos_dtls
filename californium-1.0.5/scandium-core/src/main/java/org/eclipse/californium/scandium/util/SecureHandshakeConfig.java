/*******************************************************************************
 * Copyright (c) 2015 Robert Harder and others
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Robert Harder - initial creation (http://iharder.sourceforge.net/current/java/SecureHandshakeConfig/)
 *                    Rob has provided explicit consent to re-distribute the code under EPL/EDL
 *                    (documented in CQ 9648 - http://dev.eclipse.org/ipzilla/show_bug.cgi?id=9648)
 *    Kai Hudalla (Bosch Software Innovations GmbH) - add option for preventing padding
 ******************************************************************************/
package org.eclipse.californium.scandium.util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.ByteBuffer;
import org.eclipse.californium.scandium.dtls.Handshaker;
import java.util.Random;
import java.util.Arrays;

/**
 * Rikard: Class for holding configuration information for the Secure handshake functionality
 * This is used by the Client and Server applications.
 *
 */
public class SecureHandshakeConfig
{
	//Rikard: Enable the Secure handshake functionality
	public static boolean secureHandshake = true;
	//Rikard: Enable performance timestamp outputs
	public static boolean performanceTimestamp = true;
	//Rikard: Size of anti-replay sliding window for Secure handshake Sequence number
	public static int AntiReplaySlidingWindowSize = 1024;

	//Rikard: TA IP
	String TA_IP;

	//Rikard: TA Port
	int TA_Port;

	//Rikard: Master Key (set by Server)
	public static byte[] K_M = new byte[32];
	
	public static byte[] token = new byte[40];

	private static SecureHandshakeConfig instance;

	private SecureHandshakeConfig() {

	}

	public static SecureHandshakeConfig getInstance() {

		if(instance == null) {
			instance = new SecureHandshakeConfig();
		}
		return instance;
	}

	//Rikard: Set the Master Key to be used.
	public void setK_M(byte[] K_M) {
		this.K_M = Arrays.copyOf(K_M, K_M.length);
	}

	//Rikard: Returns the master key to be used
	public byte[] getK_M()
	{
		return K_M;
	}

	//Rikard: These four getters/setters are for excluding extension from communication with TA)
	//Rikard: Set TA IP
	public void setTA_IP(String TA_IP)
	{
		this.TA_IP = TA_IP;
	}

	//Rikard: Set TA Port
	public void setTA_Port(int TA_Port)
	{
		this.TA_Port = TA_Port;
	}

	//Rikard: Get TA IP
	public String getTA_IP()
	{
		return TA_IP;
	}

	//Rikard: Get TA Port
	public int getTA_Port()
	{
		return TA_Port;
	}
    
	//Hamid: Sets the handshake token
	public void setToken (byte [] token)
	{
	    	this.token = Arrays.copyOf(token, token.length);
	}

	//Hamid: Gets the handshake token
	public byte[] getToken()
	{
		return token;
	}

}	// end class SecureHandshakeConfig

