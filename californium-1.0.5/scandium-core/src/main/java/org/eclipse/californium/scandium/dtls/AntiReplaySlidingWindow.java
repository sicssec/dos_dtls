package org.eclipse.californium.scandium.dtls;

public class AntiReplaySlidingWindow {
      
      private int windowSize;
      
      private long wLeft;
      
      private long maxSequenceNumber = (new Double (Math.pow(2, 32) - 1)).longValue();
      
      private byte[] slidingWindow;
    
      public AntiReplaySlidingWindow(int windowSize) {
      
	      this.windowSize = windowSize;
	      this.wLeft = 0;
	
	      int numBytes = windowSize / 8;
	      if (windowSize % 8 != 0)
		    numBytes++;
	
	      slidingWindow = new byte[numBytes];
	      for (byte bi : slidingWindow)
		      bi = 0;
      
      }
      
      private boolean isBitSet(long bit) {
	    int index = ((int) bit) / 8;  // Get the index of the array for the byte with this bit
	    int bitPosition = ((int) bit) % 8;  // Position of this bit in a byte

	    return (slidingWindow[index] >>> bitPosition & 1) == 1;
      }
      
      private void setBit(long bit) {
	    int index = ((int) bit) / 8;  // Get the index of the array for the byte with this bit
	    int bitPosition = ((int) bit) % 8;  // Position of this bit in a byte

	    slidingWindow[index] = (byte)(slidingWindow[index] | (1 << bitPosition));
      }
      
      // Check if the ClientHello message is too old
      public synchronized boolean isSequenceNumberValid(long sequenceNumber) {
	      if (sequenceNumber < wLeft)
		    return false;
	      else return true;
      }
      
      // Check whether a ClientHello message with the same sequence number has not been previously received
      public synchronized boolean isSequenceNumberFresh(long sequenceNumber) {
	      if (sequenceNumber > (wLeft + windowSize - 1))
		    return true;
		    
	      else {
		    long bit = sequenceNumber - wLeft;
		    if (isBitSet(bit) == true)
			  return false;
		    else
			  return true;
	      }
      }
       
      // Bit-shift the sliding window of wNew positions
      private void updateWindow(long wNew) {
      
	    // Each step of the outer loop bit-shifts the sliding window of one position
	    for (int step = 0; step < wNew; step++) {
		  
		  // Bit-shift the sliding window of one position
		  for (int i = 0; i < slidingWindow.length; i++) {
			slidingWindow[i] = (byte)(slidingWindow[i] >>> 1);
		  
			// Move the LSB of next byte to the MSB position of the current one, in case such a bit is set
			if (i < slidingWindow.length - 1) {
			      if ((slidingWindow[i+1] & 1) == 1) // The LSB is set
				    slidingWindow[i] = (byte)(slidingWindow[i] | (1 << 7));
			}
		  }

	    }
      
      } 
       
      // Mark a given sequence number as received, and possibly update the sliding window
      public synchronized void update(long sequenceNumber) {
	      // This is possible (although unlikely) in case of multiple consecutive updates by checkHighSequenceNumber()
	      if (sequenceNumber < wLeft)
		  return;
      
	      // The sequence number falls within the sliding window
	      if (sequenceNumber >= wLeft && sequenceNumber < (wLeft + windowSize)) {
		  long bit = sequenceNumber - wLeft;
		  setBit(bit);
		  return;
	      }
	      
	      // The sequence number falls outside the sliding window, i.e. sequenceNumber >= (wLeft + windowSize)
	      long wNew = sequenceNumber - windowSize + 1;
	      updateWindow(wNew - wLeft);
	      wLeft = wNew;
	      long bit = sequenceNumber - wLeft;
	      setBit(bit);

      }
      
}
