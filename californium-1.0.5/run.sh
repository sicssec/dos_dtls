#Starting Server
java -jar demo-apps/run/cf-helloworld-server-1.0.5.jar

#Starting TA
java -jar demo-apps/run/cf-trustanchor-server-1.0.5.jar

#Starting Client and performing request to Server
#The Client will get key material from the TA
java -jar demo-apps/run/cf-helloworld-client-1.0.5.jar coaps://127.0.0.1/helloWorld
