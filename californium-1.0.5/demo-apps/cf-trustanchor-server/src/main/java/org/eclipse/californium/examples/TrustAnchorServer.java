/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Kai Hudalla (Bosch Software Innovations GmbH) - add endpoints for all IP addresses
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;


// Random
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

//Rikard: Added following imports
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.eclipse.californium.scandium.util.SecureHandshakeConfig;
import java.nio.ByteBuffer;
import org.eclipse.californium.scandium.dtls.Handshaker;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

/*
 * Rikard: Trust anchor server for use with Secure handshake-enabled client and servers.
 * Suitable applications for that are the cf-helloworld-server and cf-helloworld-client
 *
 * Note that currently the TA supports providing key material for 1 server (via the resource "keyMaterial").
 * To make it able to produce key material for another server create a new resource like keyMaterial,
 * but have it calculate K_S from a different K_MS (created from a different Seed and K_M). Those values must
 * of course match what is set on the other server.
 *
*/
public class TrustAnchorServer extends CoapServer {

	//Rikard: Add 1000 to use different from normal CoAP port
	private static final int COAP_PORT = 1000 + NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);

	//Rikard: Read CoAP secure port from configuration (and add 1000 to use different from normal secure CoAP port)
	private static final int COAP_SECURE_PORT = 1000 + NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_SECURE_PORT);

	/* Rikard: Defines Master Key for Secure handshake functionality.
 	This must match what is set in the server used (cf-helloworld-server). */
	byte[] K_M = "0123456789ABCDEF0123456789ABCDEF".getBytes();

    /*
     * Application entry point.
     */
    public static void main(String[] args) {

	//Rikard: Retrieve configuration for Secure handshake extension
	SecureHandshakeConfig secureHandshakeConfig = SecureHandshakeConfig.getInstance();

	/* Rikard: PSK and identity for use with DTLS (must match other parties (client)). */
	String identity = "Client_identity";
	String psk = "secretPSK";
	
	//Rikard: Do not use Secure handshake for the TA itself
	secureHandshakeConfig.secureHandshake = false;
        
        try {

            // create server
            TrustAnchorServer server = new TrustAnchorServer();

	    // Rikard: Start DTLS endpoint for server
            InetSocketAddress localAddressSecure = new InetSocketAddress("0.0.0.0", COAP_SECURE_PORT);
	    Builder builder = new DtlsConnectorConfig.Builder(localAddressSecure);
	    builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
	    CoapEndpoint secureEndpoint = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard());
	    server.addEndpoint(secureEndpoint);

            // add endpoints on all IP addresses
            server.addEndpoints();
            server.start();

        } catch (SocketException e) {
            System.err.println("Failed to initialize server: " + e.getMessage());
        }
    }

	/**
	* Add individual endpoints listening on default CoAP port on all IPv4 addresses of all network interfaces.
	*/
	private void addEndpoints() {
		for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
			// only binds to IPv4 addresses and localhost
			if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
				InetSocketAddress bindToAddress = new InetSocketAddress(addr, COAP_PORT);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
	}

	/*
	* Constructor for a new Key Material server. Here, the resources
	* of the server are initialized.
	*/
	public TrustAnchorServer() throws SocketException {

		// provide an instance of a Hello-World resource
		add(new KeyMaterialResource());
	}

	/*
	* Definition of the Key Material Resource
	*/
	class KeyMaterialResource extends CoapResource {
	
		public KeyMaterialResource() {
			// set resource identifier
			super("keyMaterial");
		
			// set display name
			getAttributes().setTitle("Key Material Resource");

		}
	
		//Rikard: Counter for the last sequence number to sent
		long lastSequenceNumber = 0;

		@Override
		public void handleGET(CoapExchange exchange) {
	
			//Rikard: Print information about incoming request
			boolean usesDTLS = exchange.advanced().getEndpoint().getAddress().getPort() == COAP_SECURE_PORT;
			System.out.println("Received request for resource 'keyMaterial' (DTLS: " + usesDTLS + ")");

			//Rikard: Generate next Sequence Number for requesting clientlastSequenceNumber
			if(lastSequenceNumber == java.lang.Long.MAX_VALUE)
			{
				lastSequenceNumber = 0; //Rikard: TODO: Provide server with a new K_M else 
			}
			else
			{	
				lastSequenceNumber++;
			}
			
			long sequenceNumber = lastSequenceNumber;
			String sequenceNumberStr = String.valueOf(sequenceNumber);

			byte[] nonce = ByteBuffer.allocate(8).putLong(sequenceNumber).array();
			
			System.out.println("");
			// TA Token Calculation
			
			byte[] token = new byte[40]; // 8 bytes Nonce + 32 bytes mac

			byte[] MAC = new byte[32]; //Mac

			try {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] nonceHash = digest.digest(nonce);
				MAC = HMAC(K_M, nonceHash);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			System.arraycopy(nonce, 0, token, 0, 8);
			System.arraycopy(MAC, 0, token, 8, 32);

			// respond to the request
			exchange.respond(DatatypeConverter.printHexBinary(token)); //Hamid: Only the token is needed
		}


		public  byte[] getRandomBytes(int size) {
			Random r = new Random();
			byte[] sb = new byte[size];
			r.nextBytes(sb);
			return sb;
		}


	}

	//Hamid: HMAC method
	private byte[] HMAC(byte[] K_M, byte[] data) throws NoSuchAlgorithmException {
		PrintBytes("HMAC:Input K_M",K_M);
		PrintBytes("HMAC:Input data",data);
		try {
			Mac mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKeySpec = new SecretKeySpec(K_M, "HmacSHA256");
			mac.init(secretKeySpec);
			byte[] digest = mac.doFinal(data);
			return digest;
		} catch (InvalidKeyException e) {
			throw new RuntimeException("Invalid key exception while converting to HMac SHA256");
		}
	}

	//Hamid
	private void PrintBytes(String name, byte[] data) {
		System.out.print("[The content of the " + name + "]\n{\n");
		int i = 1;
		for (byte bi : data) {

			System.out.format("0x%x ", bi);
			if (i == 4) {

				System.out.print("\n");
				i = 0;

			}
			i++;
		}
		System.out.print("\n}\n");
	}
}
