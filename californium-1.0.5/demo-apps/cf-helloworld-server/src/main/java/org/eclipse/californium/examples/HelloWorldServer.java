/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Kai Hudalla (Bosch Software Innovations GmbH) - add endpoints for all IP addresses
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;

//Rikard: Added following imports
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.eclipse.californium.scandium.util.SecureHandshakeConfig;

/*
 * Rikard: Simple server for providing Hello World resource to GET requests
 * Has support for the Secure handshake extension functionality
 * Uses the Trust Anchor in cf-trustanchor-server
 *
*/

public class HelloWorldServer extends CoapServer {

	private static final int COAP_PORT = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);

	//Rikard: Read CoAP secure port from configuration
	private static final int COAP_SECURE_PORT = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_SECURE_PORT);

    /*
     * Application entry point.
     */
    public static void main(String[] args) {

	//Rikard: Retrieve configuration for Secure handshake extension
	SecureHandshakeConfig secureHandshakeConfig = SecureHandshakeConfig.getInstance();

	/* Rikard: PSK and identity for use with DTLS (must match other parties (client)). */
	String identity = "Client_identity";
	String psk = "secretPSK";

	/* Rikard: Sets Master Key for Secure handshake functionality.
 	This must match what is set in the TA server used (cf-trustanchor-server). */
	byte[] K_M = "0123456789ABCDEF0123456789ABCDEF".getBytes();
	secureHandshakeConfig.setK_M(K_M);
       
        try {

            // create server
            HelloWorldServer server = new HelloWorldServer();

	    // Rikard: Start DTLS endpoint for server
            InetSocketAddress localAddressSecure = new InetSocketAddress("0.0.0.0", COAP_SECURE_PORT);
	    Builder builder = new DtlsConnectorConfig.Builder(localAddressSecure);
	    builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
	    CoapEndpoint secureEndpoint = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard());
	    server.addEndpoint(secureEndpoint);

            // add endpoints on all IP addresses
            server.addEndpoints();
            server.start();

        } catch (SocketException e) {
            System.err.println("Failed to initialize server: " + e.getMessage());
        }
    }

    /**
     * Add individual endpoints listening on default CoAP port on all IPv4 addresses of all network interfaces.
     */
    private void addEndpoints() {
    	for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
    		// only binds to IPv4 addresses and localhost
			if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
				InetSocketAddress bindToAddress = new InetSocketAddress(addr, COAP_PORT);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
    }

    /*
     * Constructor for a new Hello-World server. Here, the resources
     * of the server are initialized.
     */
    public HelloWorldServer() throws SocketException {
        
        // provide an instance of a Hello-World resource
        add(new HelloWorldResource());
    }

    /*
     * Definition of the Hello-World Resource
     */
    class HelloWorldResource extends CoapResource {
        
        public HelloWorldResource() {
            
            // set resource identifier
            super("helloWorld");
            
            // set display name
            getAttributes().setTitle("Hello-World Resource");
        }

	int count = 0;

        @Override
        public void handleGET(CoapExchange exchange) {

            //Rikard: Print information about incoming request
	    count++;
            boolean usesDTLS = exchange.advanced().getEndpoint().getAddress().getPort() == COAP_SECURE_PORT;
            System.out.println("Received request for resource 'helloWorld' (DTLS: " + usesDTLS + ") #" + count);
            System.out.println("");

            // respond to the request
            exchange.respond("Hello World! #" + count);
        }
    }
}
