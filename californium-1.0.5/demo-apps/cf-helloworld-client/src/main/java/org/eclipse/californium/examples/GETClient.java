/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;

//Rikard: Added imports
import java.net.InetSocketAddress;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.eclipse.californium.scandium.util.SecureHandshakeConfig;
import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;

/*
 * Rikard: Simple client for performing CoAP(s) GET requests
 * Has support for the Secure handshake extension functionality
 * Uses the Trust Anchor in cf-trustanchor-server
 *
*/
public class GETClient {

	//Rikard: Set secure port for TA by reading CoAP secure port from configuration and adding 1000 (to use different from the normal secure CoAP port)
	private static final int COAP_TA_SECURE_PORT = 1000 + NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_SECURE_PORT);

	/* Rikard: URI to the TA. This works with how the cf-trustanchor-server is configured currently (when running from the same device).
	The IP should be changed if needed when the TA is on another device. */
	private static final String TA_IP = "127.0.0.1";
	private static final int TA_Port = COAP_TA_SECURE_PORT;
	private static final String COAP_TA_URI = "coaps://" + TA_IP + ":" + TA_Port + "/keyMaterial";

	/*
	 * Application entry point.
	 * 
	 */	
	public static void main(String args[]) {

		//Rikard: Retrieve configuration for Secure handshake extension
		SecureHandshakeConfig secureHandshakeConfig = SecureHandshakeConfig.getInstance();

		/* Rikard: PSK and identity for use with DTLS towards server (must match other parties (server).
		Note that currently the same identity and PSK are used for the connection
		client<->TA and client<->server, these can of course be different if desired. */
		String identity = "Client_identity";
		String psk = "secretPSK";

		//Rikard: Sets the TA IP and Port so the extension will not be applied to communication with it
		secureHandshakeConfig.setTA_IP(TA_IP);
		secureHandshakeConfig.setTA_Port(TA_Port);
		
		URI uri = null; // URI parameter of the request
		
		if (args.length > 0) {
			
			// input URI from command line arguments
			try {
				uri = new URI(args[0]);
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
				System.exit(-1);
			}
			
			CoapClient client = new CoapClient(uri);

			//Rikard: Check if this connection should use DTLS
			if(uri.toString().contains("coaps")) {
				//Rikard: First perform request for key material to TA
				KeyMaterial result = performKeyRequest();

				secureHandshakeConfig.setToken(result.token);

				if(secureHandshakeConfig == null)
					System.out.println("secureHandshakeConfig is null");

				//Rikard: Prepare the DTLS connection to the Server
				System.out.println("Performing request against Server with Key material from TA to generate MAC.");
				DTLSConnector dtlsConnector;
				DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(0));
				builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
				dtlsConnector = new DTLSConnector(builder.build(), null);
				client.setEndpoint(new CoapEndpoint(dtlsConnector, NetworkConfig.getStandard()));
			}

			CoapResponse response = client.get();
			
			if (response != null) {

				// access advanced API with access to more details through .advanced()
				System.out.println(Utils.prettyPrint(response));
				System.out.println("");
				System.exit(0); //Rikard: Close client after request
			} else {
				System.out.println("No response received.");
			}
			
		} else {
			// display help
			System.out.println("Californium (Cf) GET Client");
			System.out.println("(c) 2014, Institute for Pervasive Computing, ETH Zurich");
			System.out.println();
			System.out.println("Usage: " + GETClient.class.getSimpleName() + " URI");
			System.out.println("  URI: The CoAP(s) URI of the remote resource to GET");
		}
	}

	/* Rikard, Hamid: Method to perform request for Session Key and Sequence Number from the TA */
	public static KeyMaterial performKeyRequest()
	{
		System.out.println("Performing request for Key Material from TA: " + COAP_TA_URI);

		//Rikard: Retrieve configuration for Secure handshake extension
		SecureHandshakeConfig secureHandshakeConfig = SecureHandshakeConfig.getInstance();

		/* Rikard: PSK and identity for use with DTLS towards TA (must match other parties (TA)). */
		String identity = "Client_identity";
		String psk = "secretPSK";

		CoapClient client = new CoapClient(COAP_TA_URI);

		//Rikard: Configure the DTLS connection.
		DTLSConnector dtlsConnector;
		DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(0));
		builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
		dtlsConnector = new DTLSConnector(builder.build(), null);
		client.setEndpoint(new CoapEndpoint(dtlsConnector, NetworkConfig.getStandard()));

		CoapResponse response = client.get();

		KeyMaterial result = null;
		if (response != null) {

			String responseData = response.getResponseText();
	
			System.out.println("Received response from TA: " + responseData);
			System.out.println(Utils.prettyPrint(response));

			//Rikard: Parse response from TA with Sequence Number and Session Key
			try {
				byte[] token = DatatypeConverter.parseHexBinary(responseData);

				//Rikard: Set to return K_S and Sequence Number to be used for the connection to the Server
				result = new KeyMaterial(token);
			} catch (Exception e) {
				System.out.println("Failed to parse response from TA!");
			}

		} else {
			System.out.println("No response received.");
		}

		return result;
	}

	//Rikard: Class to hold Key material from TA
	public static class KeyMaterial {
		public static byte[] token;

		public KeyMaterial(byte[] token) {
			PrintBytes("Token",token);
			this.token = Arrays.copyOf(token, token.length);
		}

		private void PrintBytes(String name, byte[] data) {
			System.out.print("[The content of the " + name + "]\n{\n");
			int i = 1;
			for (byte bi : data) {

				System.out.format("0x%x ", bi);
				if (i == 4) {

					System.out.print("\n");
					i = 0;

				}
				i++;
			}
			System.out.print("\n}\n");
		}
	}

}
