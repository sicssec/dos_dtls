# Description #

This repository contains a proof-of-concept implementation of a countermeasure for Denial of Service attack against the DTLS handshake.

This implementation builds on the libraries Californium/Scandium 1.0.5, and considers a CoAP client and a CoAP server running DTLS 1.2.

The approach is specified in the Internet Draft available at https://datatracker.ietf.org/doc/draft-tiloca-tls-dos-handshake/


# Compiling #

To compile the code, execute the following command:

mvn clean install -DskipTests


# Instructions for running code #

Note: Specify the following command-line option when running Java 9 or more recent versions.

--add-modules java.xml.bind

### Starting Server ###
java -jar californium-1.0.5/demo-apps/run/cf-helloworld-server-1.0.5.jar

### Starting Trust Anchor ###
java -jar californium-1.0.5/demo-apps/run/cf-trustanchor-server-1.0.5.jar

### Starting Client and performing request to Server ###
java -jar californium-1.0.5/demo-apps/run/cf-helloworld-client-1.0.5.jar coaps://DTLS_SERVER_IP_ADDRESS/helloWorld
